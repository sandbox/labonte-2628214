<?php
/**
 * @file
 * VSM SearchResult implementation.
 */

namespace publicplan\wss\vsm;

use publicplan\wss\base\SearchResult;

class Result extends SearchResult {

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Parse result data.
   *
   * @param array $raw
   *   Optional raw data array. If omitted, $this->raw will be used.
   *
   * @return \publicplan\wss\vsm\Result
   *   Returns iteself.
   */
  public function parse($raw) {
    $this->raw = $raw;
    // Extract data defined via $this->format_structure.
    $this->data['title'] = (string) $raw->T;
    $this->data['uri'] = (string) $raw->U;
    $this->data['snippet'] = (string) $raw->S;
    foreach ($raw->attributes() as $name => $value) {
      if ($name == 'MIME') {
        $this->data['mime'] = (string) $value;
      }
    }
    foreach ($raw->FS->attributes() as $key => $value) {
      if ($key == 'VALUE') {
        $date = new \DateTime($value);
        $timestamp = $date->getTimestamp();
        $this->data['date'] = format_date($timestamp, $type = 'medium');
      }
    }

    return $this;
  }

  /**
   * Return the resource location/URI as string.
   *
   * @return string
   *   Provider URL of for this result/resource.
   */
  public function getResourceLocation($resource_id = NULL) {
    
  }
  
  public function getServiceId() {
    return $this->getResourceLocation();
  }
}
