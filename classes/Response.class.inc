<?php
/**
 * @file
 * VSM SearchResponse implementation.
 */

namespace publicplan\wss\vsm;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {

  /**
   * Appropriate service name.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Parse common response data.
   *
   * @return \publicplan\wss\vsm\Response
   *   Returns itself.
   */
  protected function parseResponseData() {
    if (!($this->raw = simplexml_load_string($this->body))) {
      throw new \RuntimeException(t('VSM service temporarily unavailable'));
    }
    $this->data['query'] = (string) $this->raw->Q;
    $this->data['total'] = (int) $this->raw->RES->M;
    $this->data['start'] = $this->request->getParameter(Service::PARAM__START);
    $this->data['num'] = $this->request->getParameter(Service::PARAM__NUM);
    $this->data['competencies'] = $this->parseCompetencies();

    return $this;
  }

  /**
   * Return array of VSM results.
   *
   * @return array
   *   Array of VSM result arrays.
   *
   * @throws \RuntimeException
   */
  protected function extractResults() {
//    if (empty($this->data['total']) || $this->data['total'] < 1) {
//      throw new \RuntimeException(t('Missing hits.'));
//    }
    $results = array();
    foreach ($this->raw->RES->R as $result) {
      $results[] = $result;
    }

    return $results;
  }
  
  private function parseCompetencies() {
    global $base_url;

    if (!isset($this->raw->competences) || !isset($this->raw->competences->competence)) {
      return array();
    }

    $html_decode_flags = defined('ENT_XML1') ? ENT_XML1 : (ENT_COMPAT | ENT_HTML401);
    $html_decode_charset = 'UTF-8';
    
    $competencies = array();
    foreach ($this->raw->competences->competence as $competence) {
      $authority = array(
        'id' => (int) $competence->authority->id,
        'name' => html_entity_decode($competence->authority->name, $html_decode_flags, $html_decode_charset),
        'street' => html_entity_decode($competence->authority->street . ($competence->authority->houseNumber ? " {$competence->authority->houseNumber}" : ''), $html_decode_flags, $html_decode_charset),
        'postcode' => html_entity_decode($competence->authority->postcode, $html_decode_flags, $html_decode_charset),
        'city' => html_entity_decode($competence->authority->city, $html_decode_flags, $html_decode_charset),
        'phone' => html_entity_decode($competence->authority->phone, $html_decode_flags, $html_decode_charset),
        'fax' => html_entity_decode($competence->authority->fax, $html_decode_flags, $html_decode_charset),
        'email' => html_entity_decode($competence->authority->emailAddress, $html_decode_flags, $html_decode_charset),
        'url' => urldecode($competence->authority->websiteURL),
      );
      $authority['request_url'] = Service::getSetting(Service::SETTING__OST_ADDRESS);
      if (substr($authority['request_url'], 0, 1) === '/') {
        $authority['request_url'] = $base_url . $authority['request_url'];
      }
      $authority['request_url'] .= "?authority_id={$authority['id']}";
      $competencies[] = array(
        'leikaKey' => html_entity_decode($competence['leikaKey'], $html_decode_flags, $html_decode_charset),
        'agsKey' => html_entity_decode($competence['agsKey'], $html_decode_flags, $html_decode_charset),
        'service_description' => $competence['description'],
        'commune' => $competence['commune'],
        'plz' => html_entity_decode($competence['plz'], $html_decode_flags, $html_decode_charset),
        'short_text' => html_entity_decode($competence->short_text, $html_decode_flags, $html_decode_charset),
        'full_text' => html_entity_decode($competence->full_text, $html_decode_flags, $html_decode_charset),
        'legal_basis' => html_entity_decode($competence->legal_basis, $html_decode_flags, $html_decode_charset),
        'course_of_the_procedure' => html_entity_decode($competence->course_of_the_procedure, $html_decode_flags, $html_decode_charset),
        'fees' => html_entity_decode($competence->fees, $html_decode_flags, $html_decode_charset),
        'makespan' => html_entity_decode($competence->makespan, $html_decode_flags, $html_decode_charset),
        'respites' => html_entity_decode($competence->respites, $html_decode_flags, $html_decode_charset),
        'documents' => html_entity_decode($competence->required_documents, $html_decode_flags, $html_decode_charset),
        'requirements' => html_entity_decode($competence->requirements, $html_decode_flags, $html_decode_charset),
        'forms' => html_entity_decode($competence->forms, $html_decode_flags, $html_decode_charset),
        'further_information' => html_entity_decode($competence->further_information, $html_decode_flags, $html_decode_charset),
        'specialities' => html_entity_decode($competence->specialities, $html_decode_flags, $html_decode_charset),
        'service_directive_applicable' => html_entity_decode($competence->service_directive_applicable, $html_decode_flags, $html_decode_charset),
        'authority' => $authority,
      );
    }

    return $competencies;
  }
}
