<?php
/**
 * @file
 * VSM Request implementation.
 */

namespace publicplan\wss\vsm;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  /**
   * Define cache table for requests of this kind.
   */
  const CACHE = 'cache_wss_vsm';

  /**
   * Identifier for this request class.
   *
   * This identifier is used for some inherited generic methods and to determine
   * the corresponding service class.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Request method (eg. GET/POST/PUT).
   *
   * @var string
   */
  protected $method = self::METHOD__GET;

  /**
   * Stores parameters in case of calling $this->setParameters().
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Parameter filter list.
   *
   * @var array
   */
  protected $parameterList = array(
    Service::PARAM__QUERY => NULL,
    Service::PARAM__AREA => array('\publicplan\wss\vsm\Request', 'filterArea'),
    Service::PARAM__LEIKA => NULL,
    Service::PARAM__AGS => NULL,
    Service::PARAM__NUM => NULL,
    Service::PARAM__SORT => NULL,
    Service::PARAM__START => NULL,
  );
  
  /**
   * Request auto-completion suggestions for leika competences.
   * 
   * @param string $term Search term.
   * @return string JSON string containing leika competences and their keys.
   */
  public function leikaAutocomplete($term) {
    $path = 'leika/request/complete-service-search?dlr=' . (
      Service::getSetting(Service::SETTING__AUTOCOMPLETION_DLR_ONLY) ? '1' : '0'
    );
    return $this->cachedAutocompletion($path, $term);
  }
  
  /**
   * Request auto-completion suggestions for communes.
   * 
   * @param string $term Search term.
   * @return string JSON string containing commune suggestions and their ags keys.
   */
  public function collectionAutocomplete($term) {
    return $this->cachedAutocompletion('index/complete-commune-search', $term);
  }
  
  /**
   * Perform auto-completion request with respect to local cache.
   * 
   * @param string $path
   *   Relative (to VSM instance) path to autocompletion.
   * @param string $term
   *   Incomplete search term.
   * 
   * @return array
   *   Numerical array of suggestions.
   * 
   * @throws \RuntimeException
   */
  protected function cachedAutocompletion($path, $term) {
    $base_url = Service::getSetting(Service::SETTING__SEARCH_URI);
    $path = (substr($base_url, -1) !== '/' ? '/' : '') . $path;
    $path .= strstr($path, '?') === FALSE ? '?' : '&';
    $this->setUrl($base_url . $path . $this->urlEncodeComponent('term', $term));
    // Check whether to use the cache:
    $cid = $this->getCid();
    // Try loading cached response:
    if (isset($cid) && ($cached_response = cache_get($cid, self::CACHE))) {
      $raw_response = $cached_response->data;
    }
    // Stage a real request:
    else {
      $raw_response = drupal_http_request($this->getUrl(), $this->getOptions());
      if (isset($raw_response->error) && !empty($raw_response->error)) {
        throw new \RuntimeException(t('@service returned error code @code: %msg', array(
          '@service' => $this->service,
          '@code' => $raw_response->code,
          '%msg' => $raw_response->error,
        )), $raw_response->code
        );
      }
      if (isset($cid)) {
        cache_set($cid, $raw_response, self::CACHE, $this->getService()->getCacheExpirationTime());
      }
    }
    
    return $raw_response->data;
  }

  /**
   * Assign query parameters.
   *
   * @param array $parameters
   *   Request parameters.
   *
   * @return \publicplan\wss\vsm\Request
   *   Returns itself.
   */
  public function setParameters($parameters) {
    foreach ($this->parameterList as $parameter => $filter_func) {
      if (!isset($parameters[$parameter])) {
        continue;
      }
      $this->parameters[$parameter] = is_callable($filter_func) ?
        call_user_func($filter_func, $parameter, $parameters[$parameter]) :
        $parameters[$parameter];
    }
    if (!isset($this->parameters[Service::PARAM__AREA])) {
      $this->parameters[Service::PARAM__AREA] = 'eanrw';
//      $this->parameters[Service::PARAM__AREA] = Service::getSetting(Service::SETTING__SEARCH_AREA);
    }
    if (!isset($this->parameters[Service::PARAM__NUM])) {
      $this->parameters[Service::PARAM__NUM] = Service::getSetting(Service::SETTING__SEARCH_PAGE_SIZE);
    }
    if (!isset($this->parameters[Service::PARAM__START])) {
      $this->parameters[Service::PARAM__START] = 0;
    }
    $base_url = Service::getSetting(Service::SETTING__SEARCH_URI);
    $path = (substr($base_url, -1) !== '/' ? '/' : '') . 'gsa/searchresult';

    return $this->setUrl($base_url . $path . $this->formatQueryString());
  }

  /**
   * Format a query string based on given parameters.
   *
   * @return string
   *   Returns request URL for given parameters.
   */
  public function formatQueryString() {
    $query_components = array();
    foreach ($this->parameters as $key => $value) {
      if (is_array($value)) {
        $value = \array_filter($value, array($this, 'filterEmptyString'));
      }
      if (empty($value)) {
        continue;
      }
      $query_components[] = $this->urlEncodeComponent($key, $value);
    }

    return '?' . implode('&', $query_components);
  }

  /**
   * Array filter callback removing empty string.
   *
   * @param mixed $value
   *   Input value.
   *
   * @return bool
   *   TRUE for non-empty values.
   */
  public function filterEmptyStrings($parameter, $value) {
    return !empty($value);
  }

  public function filterArea($parameter, $value) {
    if (empty($value)) {
      return 'eanrw';
    }
    elseif (!strstr($value, 'eanrw')) {
      return 'eanrw|' . $value;
    }

    return $value;
  }
}
