<?php
/**
 * @file
 * VSM Service implementation.
 */

namespace publicplan\wss\vsm;

use publicplan\wss\base\SearchService;

class Service extends SearchService {

  /**
   * Unique WSS identifier.
   */
  const NAME = 'vsm';

  /**
   * Search param: Query string.
   */
  const PARAM__QUERY = 'q';

  /**
   * Search param: Area/collection.
   */
  const PARAM__AREA = 'area';

  /**
   * Search param: AGS key.
   */
  const PARAM__AGS = 'ags';

  /**
   * Search param: Leika key.
   */
  const PARAM__LEIKA = 'leika';

  /**
   * Search param: File type.
   */
  const PARAM__FILETYPE = 'as_filetype';

  /**
   * Search param: Site.
   */
  const PARAM__SITE = 'as_sitesearch';

  /**
   * Search param: Date range.
   */
  const PARAM__DATERANGE = 'daterange';

  /**
   * Search param: Start/offset.
   */
  const PARAM__START = 'start';

  /**
   * Search param: Num/limit.
   */
  const PARAM__NUM = 'num';

  /**
   * Search param: Sorting.
   */
  const PARAM__SORT = 'sort';
  
  /**
   * Search area: Land/alles.
   */
  const AREA_LAND = 'land';

  /**
   * Search area: Finanzen.
   */
  const AREA_FM = 'fm';
  
  /**
   * Search area: Justiz.
   */
  const AREA_JM = 'jm';
  
  /**
   * Search area: Landesrechnungshof.
   */
  const AREA_LRH = 'lrh';
  
  /**
   * Search area: Landtag.
   */
  const AREA_LT = 'lt';
  
  /**
   * Search area: Arbeit, Integration und Soziales.
   */
  const AREA_MAIS = 'mais';
  
  /**
   * Search area: Bundesangelegenheiten, Europa und Medien.
   */
  const AREA_MBEM = 'mbem';
  
  /**
   * Search area: Bauen, Wohnen, Stadtentwicklung und Verkehr.
   */
  const AREA_MBWSV = 'mbwsv';
  
  /**
   * Search area: Familie, Kinder, Jugend, Kultur und Sport.
   */
  const AREA_MFKJKS = 'mfkjks';
  
  /**
   * Search area: Gesundheit, Emanzipation, Pflege und Alter.
   */
  const AREA_MGEPA = 'mgepa';
  
  /**
   * Search area: Inneres und Kommunales.
   */
  const AREA_MIK = 'mik';
  
  /**
   * Search area: Innovation, Wissenschaft und Forschung.
   */
  const AREA_MIWF = 'miwf';

  /**
   * Search area: Klimaschutz, Umwelt, Landwirtschaft, Natur- und Verbraucherschutz.
   */
  const AREA_MKULNV = 'mkulnv';
  
  /**
   * Search area: Schule und Weiterbildung.
   */
  const AREA_MSW = 'msw';
  
  /**
   * Search area: Wirtschaft, Energie, Industrie, Mittelstand und Handwerk.
   */
  const AREA_MWEIMH = 'mweimh';
  
  /**
   * Search area: Staatskanzlei.
   */
  const AREA_STK = 'stk';
  
  /**
   * Sorting: By relevance.
   */
  const SORT_BY_RELEVANCE = 'rel';

  /**
   * Sorting: By date (ascending).
   */
  const SORT_BY_DATE_ASC = 'dateasc';

  /**
   * Sorting: By date (descending).
   */
  const SORT_BY_DATE_DESC = 'datedesc';

  /**
   * Configuration setting: Search URI.
   */
  const SETTING__SEARCH_URI = 'wss_vsm_settings__search_uri';
  
  /**
   * Configuration setting: Default search area.
   */
  const SETTING__SEARCH_AREA = 'wss_vsm_settings__search_area';
  
  /**
   * Configuration setting: Number of results per page.
   */
  const SETTING__SEARCH_PAGE_SIZE = 'wss_vsm_settings__search_page_size';
  
  /**
   * Configuration setting: Text to show, if there are no search results.
   */
  const SETTING__NO_RESULT_TEXT = 'wss_vsm_settings__no_results_text';

  /**
   * Configuration setting: Whether to link the button for a competency to
   * the registered osTicket system.
   */
  const SETTING__COMPETENCY_LINK_OST = 'wss_vsm_settings__competency_link_ost';

  /**
   * Configuration setting: osTicket address.
   */
  const SETTING__OST_ADDRESS = 'wss_vsm_settings__ost_address';

  /**
   * Configuration setting: Leika auto-completion show DLR only.
   */
  const SETTING__AUTOCOMPLETION_DLR_ONLY = 'wss_vsm_settings__autocompletion_dlr_only';

  /**
   * Define the life time of cached responses.
   *
   * @var int
   */
  public static $cacheExpirationTime = 86400; // 86400 sec = 24 h

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   *   Returns an associative array of default settings (Keys: setting name;
   *   Value: Setting's default value).
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__SEARCH_URI => 'http://vsm.d-nrw.de/gsa/searchresult',
      self::SETTING__SEARCH_AREA => 'land',
      self::SETTING__SEARCH_PAGE_SIZE => 10,
      self::SETTING__NO_RESULT_TEXT => t('No results found.'),
      self::SETTING__OST_ADDRESS => '/anfrage/',
      self::SETTING__AUTOCOMPLETION_DLR_ONLY => FALSE,
    );
  }

  /**
   * Array of available sorting criteria.
   *
   * @return array
   *   Returns an associative array (Keys: Use with API; Values: Human readable
   *   criteria).
   */
  public static function getSortings() {
    return array(
      self::SORT_BY_RELEVANCE => t('Relevance'),
      self::SORT_BY_DATE_ASC => t('Date (ascending)'),
      self::SORT_BY_DATE_DESC => t('Date (descending)'),
    );
  }
  
  /**
   * Array of available search areas.
   * 
   * @return array
   *   Returns an associative array (Keys: Use with API; Values: Human readable
   *   criteria).
   */
  public static function getSearchAreas() {
    return array(
      self::AREA_LAND => t('alles'),
      self::AREA_FM => t('Finanzen'),
      self::AREA_JM => t('Justiz'),
      self::AREA_LRH => t('Landesrechnungshof'),
      self::AREA_LT => t('Landtag'),
      self::AREA_MAIS => t('Arbeit, Integration und Soziales'),
      self::AREA_MBEM => t('Bundesangelegenheiten, Europa und Medien'),
      self::AREA_MBWSV => t('Bauen, Wohnen, Stadtentwicklung und Verkehr'),
      self::AREA_MFKJKS => t('Familie, Kinder, Jugend, Kultur und Sport'),
      self::AREA_MGEPA => t('Gesundheit, Emanzipation, Pflege und Alter'),
      self::AREA_MIK => t('Inneres und Kommunales'),
      self::AREA_MIWF => t('Innovation, Wissenschaft und Forschung'),
      self::AREA_MKULNV => t('Klimaschutz, Umwelt, Landwirtschaft, Natur- und Verbraucherschutz'),
      self::AREA_MSW => t('Schule und Weiterbildung'),
      self::AREA_MWEIMH => t('Wirtschaft, Energie, Industrie, Mittelstand und Handwerk'),
      self::AREA_STK => t('Staatskanzlei'),
    );
  }
}
