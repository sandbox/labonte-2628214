(function($) {
    document.vsm = {};
    $(document).ready(function() {
        document.vsm.autocompleteObjects = {};
        $('.vsm-autocomplete').each(function() {
            var self = $(this);
            self.on('keyup', function() {
                console.log($(this).val());
                $(self.data('autocomplete-target')).prop('disabled', true);
            });
            self.autocomplete({
                source: function(req, res) {
                    $.ajax({
                        data: req,
                        method: 'get',
                        dataType: 'json',
                        url: self.data('autocomplete-url'),
                        success: function(data) { res(data); },
                        error: function() { res([]); }
                    });
                },
                appendTo: self.data('autocomplete-list'),
                select: function(event, ui) {
                    $(self.data('autocomplete-target')).val(ui.item[self.data('autocomplete-key')]).prop('disabled', false);
                },
                open: function() {
                    self.parents('form').addClass('active');
                },
                close: function() {
                    self.parents('form').removeClass('active');
                }
            });
            document.vsm.autocompleteObjects[self.attr('id')] = self;
        });

        /**
         * Alter the select callback for auto-completion of leika keywords for the menu integrated search input.
         */
        if (document.vsm.autocompleteObjects['vsm-search-simple']) {
            document.vsm.submit = function() { $('#search-block-form').submit(); };
            var inputSimpleSearch = document.vsm.autocompleteObjects['vsm-search-simple'];
            inputSimpleSearch.autocomplete('option', 'select', function(event, ui) {
                $(inputSimpleSearch.data('autocomplete-target')).val(ui.item[inputSimpleSearch.data('autocomplete-key')]).prop('disabled', false);
                setTimeout('document.vsm.submit();', 200);
            });
        }

        /**
         * Alter the select callback for auto-completion of communes to copy the AGS key.
         */
        if (document.vsm.autocompleteObjects['vsm-area']) {
            var inputCommune = document.vsm.autocompleteObjects['vsm-area'];
            inputCommune.on('keyup', function() { $('#vsmAgs').prop('disabled', true); });
            inputCommune.autocomplete('option', 'select', function(event, ui) {
                $(inputCommune.data('autocomplete-target')).val(ui.item[inputCommune.data('autocomplete-key')]).prop('disabled', false);
                $('#vsmAgs').val(ui.item.ags).prop('disabled', false);
            });
        }
    });
})(jQuery);