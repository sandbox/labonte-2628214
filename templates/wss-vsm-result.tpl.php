<?php
/**
 * @file
 * VSM search result.
 *
 * @param array $element
 *   Search result data array.
 */
global $language_url;

$uri_max_display = 35;
?>
<li class="search-result <?php print $element['#additional_classes'] ;?>">
  <h3 class="title" xml:lang="<?php print $language_url->language; ?>">
    <a href="<?php print $element['uri']; ?>" target="_blank"><?php print $element['title']; ?></a>
  </h3>
  <div class="search-snippet-info">
    <?php if (!empty($element['snippet'])): ?>
      <p class="search-snippet" xml:lang="<?php print $language_url->language; ?>"><?php print $element['snippet']; ?></p>
    <?php endif; ?>
    <?php if (!empty($element['date'])): ?>
      <p class="search-meta">
        <?php print $element['date']; ?>
        -
        <a href="<?php print $element['uri']; ?>" target="_blank">
          <?php print text_summary($element['uri'], NULL, $uri_max_display) . (strlen($element['uri']) > $uri_max_display ? '...' : ''); ?>
        </a>
      </p>
    <?php endif; ?>
  </div>
</li>
