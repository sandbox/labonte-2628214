<?php
/**
 * @file
 * VSM competency view.
 *
 * @var array $competency Competency data.
 * @var int $i Current competence index.
 */
global $base_url, $language_url;

$heading = t('Service competence for %competency in %commune is', array(
  '%competency' => $competency['service_description'],
  '%commune' => $competency['commune'],
));
$heading .= ' <span class="text-info">' . $competency['authority']['name'] . '</span>';
$address = $competency['authority']['street'] . ', ' . $competency['authority']['postcode'] . ' ' . $competency['authority']['city'];
$request_leika = "{$competency['leikaKey']} ({$competency['service_description']})";
$request_url = "{$base_url}/{$language_url->language}/anfrage-den-ea-nrw?request=".urlencode($request_leika)."&ort={$competency['agsKey']}";
?>
<div class="vsm-competency">
  <h3><?php print $heading; ?></h3>
  <a class="request-ea pull-right" href="<?php print $request_url; ?>">
    <?php print t('Request EA'); ?>
  </a>
  <div id="itnrwMap_<?php print $i; ?>" class="itnrwMap pull-left" data-itnrw-map="zoomlevel:10" data-itnrw-addresses="[<?php print $address; ?>; <?php print $competency['authority']['name']; ?>;]"></div>
  <div class="vsm-authority pull-left">
    <dl class="dl-horizontal">
      <dt><?php print t('Address:'); ?></dt>
      <dd>
        <?php print $competency['authority']['street']; ?><br/>
        <?php print $competency['authority']['postcode'] . ' ' . $competency['authority']['city']; ?>
      </dd>
      <dt><?php print t('Phone:'); ?></dt>
      <dd><?php print $competency['authority']['phone']; ?></dd>
      <?php if ($competency['authority']['fax']): ?>
        <dt><?php print t('Fax:'); ?></dt>
        <dd><?php print $competency['authority']['fax']; ?></dd>
      <?php endif; ?>
      <dt><?php print t('Email:'); ?></dt>
      <dd><?php print $competency['authority']['email']; ?></dd>
      <dt><?php print t('Internet:'); ?></dt>
      <dd>
        <a href="<?php print $competency['authority']['url']; ?>">
          <?php print $competency['authority']['url']; ?>
        </a>
      </dd>
    </dl>
  </div>
  <?php if (!empty($competency['short_text'])): ?>
    <p class="vsm-competency-short-text pull-left"><?php print $competency['short_text']; ?></p>
  <?php endif; ?>
  <div class="clearfix"></div>
  <?php if (wss_vsm_competency_has_details($competency)): ?>
    <span class="vsm-competency-details-btn pull-right" data-toggle="collapse" data-target="#vsm-competency-details-<?php print $i; ?>">
      <i class="fa fa-plus"></i>
      <?php print t('Details'); ?>
    </span>
    <div class="clearfix"></div>
    <div id="vsm-competency-details-<?php print $i; ?>" class="vsm-competency-details collapse out">
      <div>
        <p class="vsm-competency-full-text">
          <?php print $competency['full_text']; ?>
        </p>
        <dl class="dl-horizontal">
          <dt><?php print t('Legal basis:'); ?></dt>
          <dd><?php print $competency['legal_basis']; ?></dd>
          <?php if (!empty($competency['course_of_the_procedure'])): ?>
            <dt><?php print t('Course of procedure:'); ?></dt>
            <dd><?php print $competency['course_of_the_procedure']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['fees'])): ?>
            <dt><?php print t('Fees:'); ?></dt>
            <dd><?php print $competency['fees']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['makespan'])): ?>
            <dt><?php print t('Makespan:'); ?></dt>
            <dd><?php print $competency['makespan']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['respites'])): ?>
            <dt><?php print t('Respites:'); ?></dt>
            <dd><?php print $competency['respites']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['documents'])): ?>
            <dt><?php print t('Required documents:'); ?></dt>
            <dd><?php print $competency['documents']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['requirements'])): ?>
            <dt><?php print t('Requirements:'); ?></dt>
            <dd><?php print $competency['requirements']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['forms'])): ?>
            <dt><?php print t('Forms:'); ?></dt>
            <dd><?php print $competency['forms']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['further_information'])): ?>
            <dt><?php print t('Further information:'); ?></dt>
            <dd><?php print $competency['further_information']; ?></dd>
          <?php endif; ?>
          <?php if (!empty($competency['specialities'])): ?>
            <dt><?php print t('Specialities:'); ?></dt>
            <dd><?php print $competency['specialities']; ?></dd>
          <?php endif; ?>
        </dl>
      </div>
    </div>
    <div class="clearfix"></div>
  <?php endif; ?>
</div>
