<?php
/**
 * @file
 * VSM search results.
 *
 * @param \publicplan\wss\vsm\Result[] $results
 *   Array of search result objects.
 * @param array $params
 *   Search request parameters.
 * @param array $competencies
 *   Service competences if found.
 * @param int $total
 *   Total number of search results (excluding service competences).
 * @param string $no_results_text
 *   Text to display, when no results were found.
 * @param array $pagination
 *   Pagination array with following keys/values:
 *    - 'href' => The appropriate request URL
 *    - 'text' => Display text/page number
 *    - 'class' => CSS class
 */
$i = 0;
?>
<div class="vsm-results">
  <?php if ($params['start'] == 0 && $competencies): ?>
    <h2>
      <?php print t('Service competence'); ?>
      <?php if ($competencies): ?>
        <small class="vsm-total pull-right">
          <?php print t('Found %num service competences. Please specify a commune for a better result.', array('%num' => count($competencies))); ?>
        </small>
      <?php endif; ?>
    </h2>
    <?php foreach ($competencies as $cidx => $competency): ?>
      <?php if ($cidx > 4) break; ?>
      <hr/>
      <?php print theme('wss_vsm_competency', array('i' => $cidx, 'competency' => $competency)); ?>
    <?php endforeach; ?>
    <div class="clearfix"></div>
  <?php endif; ?>
  <h2>
    <?php print t('Search results'); ?>
    <small class="vsm-total pull-right"><?php print t('Found %num search results', array('%num' => $total)); ?></small>
  </h2>
  <hr/>
  <?php if (count($results) > 0): ?>
    <ol class="search-results ol-custom-structure">
      <?php foreach ($results as $result): ?>
        <?php
        $resultData = $result->getData();
        $resultData['#theme'] = 'wss_vsm_result';
        $resultData['#additional_classes' ] = $i++ % 2 === 0 ? 'odd' : 'even';
        print render($resultData);
        ?>
      <?php endforeach; ?>
    </ol>
  <?php else: ?>
    <div class="vsm-no-results">
      <?php print $no_results_text; ?>
    </div>
  <?php endif; ?>
</div>
<?php if (count($pagination) > 4): ?>
  <div class="item-list">
    <ul class="pager">
      <?php foreach ($pagination as $page): ?>
        <li class="<?php print $page['class']; ?>">
          <?php if($page['class'] === 'pager-current'): ?>
            <?php print $page['text']; ?>
          <?php else: ?>
            <a href="<?php print $page['href']; ?>"><?php print $page['text']; ?></a>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
<script type="text/javascript">
  (function($) {
    $(document).ready(function() {
      $('#vsm-results-wrapper ul.pager li a').on('click', function(event) {
        document.vsmSearch($(this).attr('href'));
        event.preventDefault();
        return false;
      });
      initNumberedLists();
      $('.pager-disabled').each(function() {
        $(this).css('opacity', 0.3).find('a').attr('href', '#').off('click');
      });
    });
  })(jQuery);
</script>
