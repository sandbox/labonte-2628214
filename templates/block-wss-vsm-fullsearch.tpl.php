<?php
/**
 * @file
 * VSM search input form.
 */

/**
 * Get template variables.
 */
global $base_url, $language_url;
?>
<form id="vsm-search-input" class="form-horizontal" role="form" accept-charset="UTF-8" action="<?php print $base_url . '/' . $language_url->language; ?>/vsm/search" method="get" >
  <?php if ((isset($element['params']['vsm-submit'])) && (empty($element['params']['search']))) { ?>
  <div class="row">
    <div class="alert alert-danger"><?php echo t('Bitte geben Sie einen Suchbegriff ein!'); ?></div>
  </div>
  <?php } ?>
  <div class="row">
    <input type="hidden" id="vsmLeika" name="leika"
      <?php if (empty($element['params']['leika'])) { print 'disabled=""'; }
            else { print 'value="' . $element['params']['leika'] . '"'; } ?>/>
    <input type="hidden" id="vsmAgs" name="ags"
      <?php if (empty($element['params']['ags'])) { print 'disabled=""'; }
            else { print 'value="' . $element['params']['ags'] . '"'; } ?>/>
    <input type="hidden" id="vsmCollection" name="area"
      <?php if (empty($element['params']['area'])) { print 'disabled=""'; }
            else { print 'value="' . $element['params']['area'] . '"'; } ?>/>
    <div class="col-md-6 col-md-offset-2">
      <div class="row">
        <div class="form-group">
          <label for="vsm-search" class="control-label col-md-3"><?php print t('Search term *:'); ?></label>
          <div class="col-md-9">
            <input type="text" id="vsm-search" name="search" placeholder="<?php print t('Search...'); ?>"
                   class="vsm-input vsm-autocomplete form-control"
                   value="<?php print $element['term']; ?>"
                   data-autocomplete-url="<?php print "$base_url/{$language_url->language}/vsm/ajax/autocomplete/leika"; ?>"
                   data-autocomplete-target="#vsmLeika"
                   data-autocomplete-key="serviceKey"
                   data-autocomplete-list="#search-leika-autocomplete2"/>
            <div id="search-leika-autocomplete2" class="ajax-autocomplete"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group">
          <label for="vsm-area" class="control-label col-md-3"><?php print t('Commune:'); ?></label>
          <div class="col-md-9">
            <input type="text" id="vsm-area" name="commune" placeholder="<?php print t('All communes in NRW'); ?>"
                   class="vsm-input vsm-autocomplete form-control"
                   <?php if (!empty($element['params']['commune'])) { print 'value="' . $element['params']['commune'] . '"'; } ?>
                   data-autocomplete-url="<?php print "$base_url/{$language_url->language}/vsm/ajax/autocomplete/collection"; ?>"
                   data-autocomplete-target="#vsmCollection"
                   data-autocomplete-key="collectionKey"
                   data-autocomplete-list="#search-collection-autocomplete"/>
            <div id="search-collection-autocomplete" class="ajax-autocomplete"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group">
          <?php /*
          <label for="vsm-sorting" class="control-label col-md-3"><?php print t('Sorting:'); ?></label>
          <div class="col-md-6">
            <select id="vsm-sorting" name="sorting" class="form-select">
              <?php foreach ($element['sortings'] as $sort_value => $sort_display): ?>
                <option value="<?php print $sort_value; ?>"
                        <?php if ($sort_value === $element['params']['sorting']) { print 'selected="selected"'; } ?>>
                  <?php print $sort_display; ?>
                </option>
              <?php endforeach; ?>
            </select>
          </div>
          */ ?>
          <div class="col-md-3 col-md-offset-3">
            <button type="submit" name="vsm-submit"<?php /* class="pull-right" */ ?>>
              <i class="fa fa-search"></i> <?php print t('Search'); ?>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
