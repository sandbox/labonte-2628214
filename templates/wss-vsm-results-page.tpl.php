<?php
/**
 * @file
 * VSM search results.
 *
 * @param string $base_url
 *   Drupal's global $base_url
 * @param array $params
 *   Internal search request parameters.
 */
$error_text = t('There was an error during your search request. Please try again later.');
?>
<div id="vsm-results-wrapper">
  <div id="vsm-results-container" style="display: none;"></div>
  <?php if (!empty($params['term'])): ?>
  <div id="vsm-loading-placeholder" class="text-center" style="display: none;">
    <p>
      <img src="<?php echo $base_url . '/' . drupal_get_path('module', 'wss_vsm'); ?>/images/loading-cube.gif"
           alt="<?php echo t('Loading...'); ?>"/>
    </p>
    <p><?php echo t('VSM is processing...'); ?></p>
  </div>
  <?php endif; ?>
</div>
<?php if (!empty($params['term'])): ?>
  <script type="text/javascript">
    (function($) {
      document.vsmSearch = function(url, parameters) {
        $.ajax(url, {
          dataType: 'html',
          method: 'get',
          data: parameters,
          beforeSend: function() {
            $('#vsm-results-container').slideUp();
            $('#vsm-loading-placeholder').show();
          },
          success: function(data) {
            $('#vsm-results-container').html(data).slideDown();
          },
          error: function() {
            $('#vsm-results-container').html('<?php print $error_text; ?>').slideDown();
          },
          complete: function() {
            $('#vsm-loading-placeholder').hide();
          }
        });
      };

      $(document).ready(function() {
        var url = '<?php echo "$base_url/vsm/ajax/search"; ?>';
        var params = <?php print json_encode($params) ?: '{}' ?>;
        document.vsmSearch(url, params);
      });
    })(jQuery);
  </script>
<?php endif; ?>
