<?php
global $base_url, $language_url;
?>
<form action="<?php echo "{$base_url}/{$language_url->language}/vsm/search"; ?>" method="get" id="search-block-form" accept-charset="UTF-8">
  <div>
    <div class="container-inline">
      <h2 class="element-invisible">Search form</h2>
      <div class="form-item form-type-textfield form-item-search-block-form">
        <label class="element-invisible" for="edit-search-block-form--2">Search </label>
        <span class="ui-helper-hidden-accessible" aria-live="polite" role="status">No search results.</span>
        <input id="leikaKey" type="hidden" name="leika"/>
        <input id="vsm-search-simple" autocomplete="off" name="search" value="" size="15" maxlength="128" class="form-text ui-autocomplete-input vsm-autocomplete" type="text"
               title="<?php echo t('Enter the terms you wish to search for.'); ?>" 
               placeholder="<?php echo t('Suchbegriff eingeben'); ?>"
               data-autocomplete-url="<?php echo "{$base_url}/{$language_url->language}/vsm/ajax/autocomplete/leika"; ?>"
               data-autocomplete-list="#wss-vsm-search-leika-autocomplete"
               data-autocomplete-target="#leikaKey"
               data-autocomplete-key="serviceKey">
      </div>
      <div class="form-actions form-wrapper" id="edit-actions">
        <input id="vsm-search-simple-submit" name="op" value="Search" class="form-submit" type="submit">
      </div>
    </div>
  </div>
  <div id="wss-vsm-search-leika-autocomplete" class="ajax-autocomplete"></div>
<!--    <ul style="display: none;" tabindex="0" id="ui-id-1" class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all"></ul>-->
<!--  </div>-->
</form>