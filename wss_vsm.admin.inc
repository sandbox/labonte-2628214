<?php
/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\vsm\Service as VsmService;

/**
 * Callback function for VSM specific administration settings.
 *
 * @return array Renderable form.
 */
function wss_vsm_admin_settings($form_id, $form_state) {
  $settings = array();
  
  $settings[VsmService::SETTING__SEARCH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Search service address'),
    '#description' => 'Syntax: http://vsm.example.com/',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => VsmService::getSetting(VsmService::SETTING__SEARCH_URI),
    '#weight' => 10,
  );
//  $settings[VsmService::SETTING__SEARCH_AREA] = array(
//    '#type' => 'select',
//    '#title' => t('Default search area'),
//    '#description' => 'Choose the default (pre-selected) search area.',
//    '#required' => TRUE,
//    '#options' => VsmService::getSearchAreas(),
//    '#default_value' => VsmService::getSetting(VsmService::SETTING__SEARCH_AREA),
//    '#weight' => 20,
//  );
  $settings[VsmService::SETTING__SEARCH_PAGE_SIZE] = array(
    '#type' => 'textfield',
    '#title' => t('Number of results per page'),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#default_value' => VsmService::getSetting(VsmService::SETTING__SEARCH_PAGE_SIZE),
    '#weight' => 30,
  );
//  $no_results = VsmService::getSetting(VsmService::SETTING__NO_RESULT_TEXT);
//  $settings[VsmService::SETTING__NO_RESULT_TEXT] = array(
//    '#type' => 'text_format',
//    '#title' => t('No results text'),
//    '#description' => "Text to show when no results where found. You can use '%term' as a placeholder for the actual search term.",
//    '#rows' => 5,
//    '#cols' => 60,
//    '#required' => TRUE,
//    '#default_value' => is_array($no_results) ? $no_results['value'] : $no_results,
//    '#format' => is_array($no_results) ? $no_results['format'] : NULL,
//    '#weight' => 40,
//  );
  $settings[VsmService::SETTING__COMPETENCY_LINK_OST] = array(
    '#type' => 'checkbox',
    '#title' => t('Let buttons link the osTicket system'),
    '#required' => FALSE,
    '#default_value' => VsmService::getSetting(VsmService::SETTING__COMPETENCY_LINK_OST),
    '#weight' => 49,
  );
  $settings[VsmService::SETTING__OST_ADDRESS] = array(
    '#type' => 'textfield',
    '#title' => t('osTicket address'),
    '#required' => TRUE,
    '#default_value' => VsmService::getSetting(VsmService::SETTING__OST_ADDRESS),
    '#weight' => 50,
  );
  $settings[VsmService::SETTING__AUTOCOMPLETION_DLR_ONLY] = array(
    '#type' => 'checkbox',
    '#title' => t('Suggest only DLR flagged leikas'),
    '#required' => FALSE,
    '#default_value' => VsmService::getSetting(VsmService::SETTING__AUTOCOMPLETION_DLR_ONLY),
    '#weight' => 60,
  );

  return system_settings_form($settings);
}
